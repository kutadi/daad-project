package helper;

import aStar.Edge;
import aStar.Node;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;

public class Parser {
    private HashMap<String, Node> nodes = new HashMap<>();

    //get all files in folder
    public HashMap<String, Node> listAllFiles(String path) {
        try (Stream<Path> paths = Files.walk(Paths.get(path))) {
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        readContent(filePath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return nodes;
    }

    //get data from files
    private void readContent(Path filePath) {
        String file = filePath.toString();

        ArrayList<Node> nodes = new ArrayList<>();// temp nodes, helps to adding edges

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;

            while ((line = br.readLine()) != null) {
                if(line.trim().length() == 0)
                    continue;

                Node node = new Node(line.trim());
                node.line = filePath.getFileName().toString().replace(".txt", "");
                nodes.add(node);
            }

            br.close();

        } catch (IOException e) {
            System.out.println("ERROR: Unable to read file " + file);
            e.printStackTrace();
        }

        //adding edges
        for(int i = 0; i < nodes.size(); i++) {
            if(this.nodes.containsKey(nodes.get(i).value)) {
                if(i == 0) {
                    this.nodes.get(nodes.get(i).value).adjacencies.add(new Edge(nodes.get(i + 1)));
                    this.nodes.get(nodes.get(i).value).line = nodes.get(i).line;
                }
                else if(i == nodes.size() - 1) {
                    this.nodes.get(nodes.get(i).value).adjacencies.add(new Edge(nodes.get(i - 1)));
                    this.nodes.get(nodes.get(i).value).line = nodes.get(i).line;
                } else {
                    this.nodes.get(nodes.get(i).value).adjacencies.add(new Edge(nodes.get(i+1)));
                    this.nodes.get(nodes.get(i).value).adjacencies.add(new Edge(nodes.get(i-1)));
                    this.nodes.get(nodes.get(i).value).line = nodes.get(i).line;
                }
            } else {
                if(i == 0) {
                    Node newNode = new Node(nodes.get(i).value);
                    newNode.adjacencies.add(new Edge(nodes.get(i + 1)));
                    newNode.line = nodes.get(i).line;
                    this.nodes.put(nodes.get(i).value, newNode);
                }
                else if(i == nodes.size() - 1) {
                    Node newNode = new Node(nodes.get(i).value);
                    newNode.adjacencies.add(new Edge(nodes.get(i - 1)));
                    newNode.line = nodes.get(i).line;
                    this.nodes.put(nodes.get(i).value, newNode);
                }
                else {
                    Node newNode = new Node(nodes.get(i).value);
                    newNode.adjacencies.add(new Edge(nodes.get(i + 1)));
                    newNode.adjacencies.add(new Edge(nodes.get(i - 1)));
                    newNode.line = nodes.get(i).line;
                    this.nodes.put(nodes.get(i).value, newNode);
                }
            }
        }
    }
}
