import aStar.Node;
import aStar.aStar;
import helper.Parser;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


public class AlgoTest {
    public static void main(String[] args) {
        Parser parser = new Parser();
        aStar aStar = new aStar();
        HashMap<String, Node> nodes = parser.listAllFiles(System.getProperty("user.dir") + "\\Lines");

        Node start;
        Node finish;
        Scanner scanner = new Scanner(System.in);

        System.out.println("=========================================");
        System.out.println("Available Stations: ");
        System.out.println("=====================");

        nodes.forEach((k, v) -> System.out.println(k));

        System.out.println("=====================");

        do {
            System.out.println("Please enter a Start point: ");
            start = nodes.get(scanner.nextLine());

        }
        while(start == null);

        System.out.println("=====================");

        do {
            System.out.println("Please enter a Finish point: ");
            finish = nodes.get(scanner.nextLine());
        }
        while(finish == null);

        System.out.println("=========================================");
        System.out.println("Start: " + start.value);
        System.out.println("Finish: " + finish.value);
        System.out.println("=========================================");

        List<Node> result = aStar.aStarSearch(nodes, start.value, finish.value);

        System.out.println("Result: ");
        for(int i = 0; i < result.size(); i++) {
            if(i != 0 && !result.get(i).line.equals(result.get(i-1).line))
                System.out.println(i+1 + ". " + result.get(i).value + "(change to " + result.get(i).line + ")");
            else
                System.out.println(i+1 + ". " + result.get(i).value + "(" + result.get(i).line + ")");
        }

        System.out.println("=========================================");
    }
}
