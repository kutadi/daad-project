package aStar;

import java.util.*;

public class aStar {
    public List<Node> aStarSearch(HashMap<String, Node> nodes, String source, String goal) {
        Set<Node> explored = new HashSet<>();

        Queue<Node> queue = new LinkedList<>();
        queue.add(nodes.get(source));

        boolean found = false;

        while (!queue.isEmpty() && !found) {
            Node current = nodes.get(queue.poll().value);
            explored.add(current);

            if (current.value.equals(goal)) {
                found = true;
            }

            //check every child of current node
            for (Edge e : current.adjacencies) {
                Node child = nodes.get(e.target.value);

                //if child node has been evaluated
                if ((explored.contains(child))) {
                    continue;
                }

                else if ((!queue.contains(child))) {
                    child.parent = current;

                    queue.add(child);
                }
            }
        }

        return printPath(nodes.get(goal));
    }

    //getting path
    private List<Node> printPath(Node target) {
        List<Node> path = new ArrayList<>();

        for (Node node = target; node != null; node = node.parent) {
            path.add(node);
        }

        Collections.reverse(path);

        return path;
    }
}