package aStar;

public class Edge{
    public final Node target;

    public Edge(Node targetNode){
        target = targetNode;
    }

    @Override
    public String toString() {
        return target.value;
    }
}