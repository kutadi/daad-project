package aStar;

import java.util.ArrayList;

public class Node {
    public final String value;
    public String line;
    public ArrayList<Edge> adjacencies = new ArrayList<>();
    public Node parent;

    public Node(String val){
        value = val;
    }

    public String toString(){
        return value;
    }
}